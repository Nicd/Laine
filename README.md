# Project abandoned

This project is abandoned. It was a fun proof of concept but not really that
useful. The code is left here only for reference.

# Laine

Laine is a minimalistic, fully static blog engine which you can read all about
at [laine.nytsoi.net](http://laine.nytsoi.net/).

## Installing for use

1. Download the zip file from the downloads section.
2. Extract Laine to `/path/to/yourblog/`.
3. Write your posts at `/path/to/yourblog/posts/`.
4. Write your pages at `/path/to/yourblog/pages/`.
5. Create index file at `/path/to/yourblog/index`.
6. Serve when hot with Nginx.

## Installing for development

Requirements: NPM, Bower.

1. Clone this repo.
2. `cd /path/to/clone`
3. `npm install`
4. `bower install`
5. Write changes.
6. `gulp` to compile development version to `dev/` and minified version to
   `dist/`.

Serving locally is done easily with
`cd /path/to/clone/dev; python -m SimpleHTTPServer 8080`. You need a server
because browsers don't like AJAX requests over the `file://` scheme.
