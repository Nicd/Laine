define(['knockout', 'text!./pagination.html', '../address-service/address-service', '../config-service/config-service'],
  function(ko, templateMarkup, addressService, configService) {

  function Pagination(params) {
    var self = this;
    self.posts = params.posts;
    self.page = params.page;

    self.postsPerPage = configService.postsPerPage;

    self.pageCount = ko.pureComputed(function() {
      return Math.ceil(self.posts().length / self.postsPerPage);
    });

    self.sanitizePage = function(page) {
      if (page < 1) {
        page = 1;
      }
      else if (page > self.pageCount()) {
        page = self.pageCount();
      }

      return page;
    };

    self.goToUrl = function(page) {
      page = self.sanitizePage(page);
      return addressService.prefixUrlTo({pageNumber: page});
    };

    self.goBackUrl = function() {
      return self.goToUrl(self.page() - 1);
    }

    self.goForwardUrl = function() {
      return self.goToUrl(self.page() + 1);
    }

    self.postsForPage = ko.pureComputed(function() {
      var start = (self.page() - 1) * self.postsPerPage;
      return self.posts().slice(start, start + self.postsPerPage);
    });
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  Pagination.prototype.dispose = function() {};

  return { viewModel: Pagination, template: templateMarkup };

});
