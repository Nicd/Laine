define(['knockout', 'text!./post-page.html', '../generic-route/generic-route', '../db/db'],
  function(ko, templateMarkup, GR, DB) {

  function PostPage(route) {
    var self = this;

    self.post = ko.pureComputed(function() {
      if (route.mode === 'post') {
        var posts = DB.postDict();
      }
      else if (route.mode === 'page') {
        var posts = DB.pageDict();
      }

      if (route.slug in posts) {
        return posts[route.slug]();
      }
      else {
        return null;
      }
    });

    GR.sync();
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  PostPage.prototype.dispose = function() {};

  return { viewModel: PostPage, template: templateMarkup };

});
