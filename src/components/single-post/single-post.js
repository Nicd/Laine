define(['knockout', 'text!./single-post.html', '../db/db', 'marked', '../address-service/address-service', '../config-service/config-service', 'hasher'],
  function(ko, templateMarkup, DB, marked, addressService, configService, hasher) {

  var disqusSub = null;

  function disqusIdentifier(post) {
    return (post.isPage()? 'page-' : 'post-') + post.slug();
  }

  function insertDisqus(post) {
    if (!_.isUndefined(window.DISQUS)) {
      resetDisqus(post);
    }
    else {
      var disqus_shortname = configService.disqusShortname;
      var disqus_title = post.title();
      var disqus_url = hasher.getURL();
      var disqus_identifier = disqusIdentifier(post);

      var dsq = document.createElement('script');
      dsq.type = 'text/javascript';
      dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    }
  }

  function resetDisqus(post) {
    DISQUS.reset({
      reload: true,
      config: function() {
        this.page.identifier = disqusIdentifier(post);
        this.page.url = hasher.getURL();
      }
    });
  }

  function SinglePost(params) {
    var self = this;

    // For some reason post-page gives params.post as an observable, not an
    // object
    if (_.isUndefined(params.post.date)) {
      self.post = params.post();
    }
    else {
      self.post = params.post;
    }

    if (!self.post.isPage()) {
      self.year = self.post.date().format('YYYY');
      self.month = self.post.date().format('MM');
    }

    self.marked = marked;
    self.AS = addressService;

    // Is this post shown as short (in pagination) or not?
    self.short = params.short;

    self.content = self.post.content;
    if (self.short) {
      self.content = self.post.shortContent;
    }

    // If this post is not fully fetched into the index yet, do that now
    if (!self.post.synced()) {
      if (!self.post.isPage()) {
        DB.loadPost(self.post.slug());
      }
      else {
        DB.loadPage(self.post.slug());
      }
    }

    // Load disqus comments if they are in use on this page
    self.useDisqus = !self.short && configService.useDisqus && (configService.pageCommenting || !self.post.isPage());

    if (self.useDisqus) {
      // Wait until the post title is available
      if (!self.post.synced()) {
        disqusSub = self.post.synced.subscribe(function (newVal) {
          if (newVal === true) {
            insertDisqus(self.post);
          }
        });
      }
      else {
        insertDisqus(self.post);
      }
    }
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  SinglePost.prototype.dispose = function() {
    if (!_.isNull(disqusSub)) {
      disqusSub.dispose();
    }
  };

  return { viewModel: SinglePost, template: templateMarkup };

});
