define(['knockout'],
  function(ko) {

  function ConfigService() {
    var self = this;

    // Load configs from variables defined in config.js
    _.merge(self, LAINE_CONFIG);

    document.title = self.blogName;
    self.baseTitle = self.blogName;

    // Load custom CSS if specified
    if (self.customCss) {
      var head  = document.getElementsByTagName('head')[0];
      var link  = document.createElement('link');
      link.id   = 'laine-custom-css';
      link.rel  = 'stylesheet';
      link.type = 'text/css';
      link.href = self.customCss;
      link.media = 'all';
      head.appendChild(link);
    }
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  ConfigService.prototype.dispose = function() {};

  return new ConfigService();

});
