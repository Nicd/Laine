define(['knockout', '../../app/routes', '../../app/router', 'hasher'],
  function(ko, routes, router, hasher) {

  function AddressService() {
    var self = this;

    self.getCurrentRoute = function() {
      return router.currentRoute();
    };

    self.urlTo = function(arguments, mode) {
      if (_.isUndefined(mode)) {
        mode = self.getCurrentRoute().mode;

        // If we are moving to the same mode, use the old arguments as a base to
        // extend with new arguments
        arguments = _.assign(_.clone(self.getCurrentRoute()), arguments);
      }

      var route = _.find(routes, function(route) {
        return route.params.mode === mode;
      });

      if (_.isUndefined(route)) {
        throw new Error('urlTo given non-existing mode!');
      }

      var url = route.url
      _.forEach(arguments, function(value, key) {
        url = url.replace('{' + key + '}', value);
        url = url.replace(':' + key + ':', value);
      });
      return url;
    };

    self.prefixUrlTo = function(arguments, mode) {
      return '/#!/' + self.urlTo(arguments, mode);
    };

    self.goTo = function(arguments, mode) {
      var url = self.urlTo(arguments, mode);
      hasher.setHash(url);
    };

    self.postUrl = function(post) {
      return self.prefixUrlTo({
        year: post.date().format('YYYY'),
        month: post.date().format('MM'),
        day: post.date().format('DD'),
        slug: post.slug()
      }, 'post');
    };

    self.pageUrl = function(page) {
      return self.prefixUrlTo({
        slug: page.slug()
      }, 'page');
    }
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  AddressService.prototype.dispose = function() {};

  return new AddressService();

});
