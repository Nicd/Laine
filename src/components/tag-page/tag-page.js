define(['knockout', 'text!./tag-page.html', '../generic-route/generic-route', '../db/db'],
  function(ko, templateMarkup, GR, DB) {

  function TagPage(route) {
    var self = this;

    self.tag = route.slug;
    self.page = ko.observable(1);
    self.posts = ko.pureComputed(function() {
      var tags = DB.tags();

      if (route.slug in tags) {
        return tags[route.slug]();
      }
      else {
        return [];
      }
    });

    GR.initPage(route, self.page);
    GR.sync();
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  TagPage.prototype.dispose = function() {};

  return { viewModel: TagPage, template: templateMarkup };

});
