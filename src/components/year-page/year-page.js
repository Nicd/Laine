define(['knockout', 'text!./year-page.html', '../generic-route/generic-route', '../db/db'],
  function(ko, templateMarkup, GR, DB) {

  function YearPage(route) {
    var self = this;

    self.year = route.year;
    self.page = ko.observable(1);
    self.posts = ko.pureComputed(function() {
      var years = DB.years();

      if (route.year in years) {
        return years[route.year]();
      }
      else {
        return [];
      }
    });

    GR.initPage(route, self.page);
    GR.sync();
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  YearPage.prototype.dispose = function() {};

  return { viewModel: YearPage, template: templateMarkup };

});
