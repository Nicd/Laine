// Node modules
var fs = require('fs'), vm = require('vm'), merge = require('deeply'), chalk = require('chalk'), es = require('event-stream');

// Gulp and plugins
var gulp = require('gulp'), rjs = require('gulp-requirejs-bundler'), concat = require('gulp-concat'), clean = require('gulp-clean'),
    replace = require('gulp-replace'), uglify = require('gulp-uglify'), htmlreplace = require('gulp-html-replace');

// Gulp minify for smallinizing our CSS
var minify = require('gulp-minify-css');

// Gulp filesize for printing sizes before and after minification
var size = require('gulp-size');

// Config
var requireJsRuntimeConfig = vm.runInNewContext(fs.readFileSync('src/app/require.config.js') + '; require;');
    requireJsOptimizerConfig = merge(requireJsRuntimeConfig, {
        out: 'scripts.js',
        baseUrl: './src',
        name: 'app/startup',
        paths: {
            requireLib: 'bower_modules/requirejs/require',
            lodashLib: 'bower_modules/lodash/dist/lodash',
            momentLib: 'bower_modules/moment/moment'
        },
        include: [
            'requireLib',
            'lodashLib',
            'momentLib',
            'components/address-service/address-service',
            'components/nav-bar/nav-bar',
            'components/home-page/home',
            'components/db/db',
            'components/generic-route/generic-route',
            'components/transfer/transfer',
            'components/single-post/single-post',
            'components/pagination/pagination',
            'components/tag-page/tag-page',
            'components/year-page/year-page',
            'components/month-page/month-page',
            'components/post-page/post-page',
            'components/config-service/config-service',
            'components/template-service/template-service'
        ],
        insertRequire: ['app/startup'],
        bundles: {
            // If you want parts of the site to load on demand, remove them from the 'include' list
            // above, and group them into bundles here.
            // 'bundle-name': [ 'some/module', 'another/module' ],
            // 'another-bundle-name': [ 'yet-another-module' ]
        }
    });

// Discovers all AMD dependencies, concatenates together all required .js files, minifies dist files
gulp.task('js', function() {
    return rjs(requireJsOptimizerConfig)
        .pipe(size({title: 'Original JS'}))
        .pipe(gulp.dest('./dev/'))
        .pipe(uglify({ preserveComments: false }))
        .pipe(size({title: 'Minified JS'}))
        .pipe(gulp.dest('./dist/'));
});

// Concatenates CSS files, rewrites relative paths to Bootstrap fonts, copies Bootstrap fonts
gulp.task('css', function() {
    var bowerCss = gulp.src('src/bower_modules/components-bootstrap/css/bootstrap.min.css')
            .pipe(replace(/url\((')?\.\.\/fonts\//g, 'url($1fonts/')),
        appCss = gulp.src('src/css/*.css');

    return es.concat(bowerCss, appCss)
        .pipe(concat('css.css'))
        .pipe(size({title: 'Original CSS'}))
        .pipe(gulp.dest('./dev/'))
        .pipe(minify())
        .pipe(size({title: 'Minified CSS'}))
        .pipe(gulp.dest('./dist/'));
});

// Copies fonts
gulp.task('fonts', function() {
    return gulp.src('./src/bower_modules/components-bootstrap/fonts/*', { base: './src/bower_modules/components-bootstrap/' })
        .pipe(gulp.dest('./dev/'))
        .pipe(gulp.dest('./dist/'));
});

// Copies index.html, replacing <script> and <link> tags to reference production URLs
gulp.task('html', function() {
    return gulp.src('./src/index.html')
        .pipe(htmlreplace({
            'css': 'css.css',
            'js': 'scripts.js'
        }))
        .pipe(gulp.dest('./dist/'))
        .pipe(gulp.dest('./dev/'));
});

// Copies misc files
gulp.task('misc', function() {
    return gulp.src('./src/config.js.dist')
        .pipe(gulp.dest('./dist/'))
        .pipe(gulp.dest('./dev/'));
});

// Removes all files from ./dist/
gulp.task('clean', function() {
    return gulp.src('./dist/**/*', { read: false })
        .pipe(clean());
});

gulp.task('default', ['html', 'js', 'css', 'fonts', 'misc'], function(callback) {
    callback();
    console.log('\nPlaced optimized files in ' + chalk.magenta('dist/')
        + ' and dev files in ' + chalk.magenta('dev/\n'));
});
