define(
  [
    'knockout',
    '../db/db',
    '../config-service/config-service',
    '../template-service/template-service'
  ], function(ko, DB, configService, templateService) {

  function GenericRoute() {
    var self = this;

    self.sync = function() {
      if (!DB.synced()) {
        DB.loadIndex();
      }
    };

    self.initPage = function(route, page) {
      if (!_.isUndefined(route.pageNumber)) {
        page(parseInt(route.pageNumber, 10));
      }
    }

    self.resolveTemplate = function(originalTemplate, templateName) {
      if (templateName in configService.customTemplates
          && configService.customTemplates[templateName]) {
        return templateService.loadTemplate(templateName, configService.customTemplates[templateName]);
      }
      else {
        return originalTemplate;
      }
    }
  }

  // This runs when the component is torn down. Put here any logic necessary to clean up,
  // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
  GenericRoute.prototype.dispose = function() {};

  return new GenericRoute();

});
